# analyzeBox-jenkins

Shared library Jenkins pour faciliter l'analyse de code webMethods avec analyzeBox 

## 0. Prérequis
Afin que les fonctions de la shared library jenkins fonctionnent correctement, il est nécessaire de disposer des plugins Jenkins ci-dessous :

| Nom plugin             | version plugin  |
|     :-------------:    | :-------------: |
| Pipeline Utility Steps |     2.40        |
| HTTP Request Plugin    |     1.8.26      |

## 1. Mode opératoire de déclaration des credentials Jenkins
Pour se faire, il est nécessaire de suivre les étapes suivantes depuis la console web jenkins:
- Se connecter à la console http://ip:port/jenkins

- Cliquer sur "Manage Jenkins" / "Manage Credentials"

![Manage Credentials](images/03_manageCredentials.png)

- Cliquer sur "Add Credentials"


![Manage Credentials](images/04_addCredentials.png)

Saisir les informations suivantes :
- __UserName__ : login de l'utilisateur
- __Password__ : mot de passe associé
- __ID__ : identifiant du credential utilisé dans les pipelines


__Pour le bon fonctionnement des pipelines, il est nécessaire de définir deux crédentials:__
- __ANALYZEBOX_USER__ : utilisateur ayant souscrit à l'offre analyzeBox
- __ANALYZEBOX_GIT__ : utilisateur GIT afin de récupérer la shared library (si nécessaire)


## 2. Mode opératoire de déclaration de la shared library "analyzeBox"

Pour se faire, il est nécessaire de suivre les étapes suivantes depuis la console web jenkins:
- Se connecter à la console http://ip:port/jenkins

- Cliquer sur "Manage Jenkins" / "Configure System"

![Configure System](images/01_declarationSharedLibrary.png)

- Se positionner dans la zone "Global Pipeline Libraries" et cliquer sur Add

![Add Library](images/02_addSharedLibrary.png)

Saisir les paramètres suivants :

- __Name__ : analyzebox (nom logique de la library)

- __Default version__ : master (branche master)

- __Retrieval Method__ : Modern SCM

- __Source Code Management__ : GIT

- __Project Repository__ : https://gitlab.com/JP1203/analyzebox-jenkins.git (URL au repository GIT)

- __Credentials__ : ANALYZEBOX_GIT (credential déclaré dans Jenkins afin d'accéder au repository si nécesssaire)


## 3. Mode opératoire d'utilisation de la shared library dans une pipeline

### 3.1 Utilisation de  la fonction d'audit de code webMethods via Analyzebox 
Pour se faire, il est nécessaire de suivre les étapes suivantes depuis la console web jenkins:
- Se connecter à la console http://ip:port/jenkins
- Se positionner sur la pipeline jenkins pour laquelle une analyze de code webMethods doit être réalisée

- Ajouter les lignes de code suivantes :
```groovy

//-Déclaration de la shared libraruy
@Library('analyzebox@master') _

pipeline {
    agent any
    stages {
        stage('Analyze-with-AnalyzeBox') {
            steps {
                //-Appel de l'audit de code webMethods
                analyzeWm(
                    analyzeBoxUserCredentialId: 'ANALYZEBOX_USER',
                    subscriptionId: 200,
                    projectId: 203,
                    packagesFolder: 'C://DEV//myProject//packages',
                    httpProxy: 'http://193.56.47.8:8080'
                )
            }
        }
    }
}

```
avec les paramètres ci-dessous :
- __analyzeBoxUserCredentialId__ : credential déclaré dans Jenkins afin de s'authentifier à analyzeBox
- __subscriptionId__ : identifiant de la souscription à analyzeBox
- __projectId__ : identifiant du projet analyzeBox
- __packagesFolder__ : répertoire contenant les packages webMethods à analyser
- __httpProxy__ : adresse du serveur proxy http (optionnel)

### 3.2 Utilisation de  la fonction de remplacement d'étiquettes

Pour se faire, il est nécessaire de suivre les étapes suivantes depuis la console web jenkins:
- Se connecter à la console http://ip:port/jenkins
- Se positionner sur la pipeline jenkins pour laquelle une analyze de code webMethods doit être réalisée

- Ajouter les lignes de code suivantes :
```groovy

//-Déclaration de la shared libraruy
@Library('analyzebox@master') _

pipeline {
    agent any
    stages {
        stage('ReplaceVariableInFile-with-AnalyzeBox') {
            steps {
                //-Appel de la fonction
                variableReplace(
                    templateFile: 'C://DEV//myProject//template.file',
                    propertiesFile: 'C://DEV//myProject//env.properties',
                    targetFile: 'C://DEV//myProject//target.file',
                    prefix: '@',
                    suffix: '@'
                )
            }
        }
    }
}

```
avec les paramètres ci-dessous :
- __templateFile__ : fichier template pour lequel les étiquettes doivent être valorisée
- __propertiesFile__ : fichier contenant les propriétés à utiliser pour la valorisation des étiquettes
- __targetFile__ : fichier cible correspondant au contenu du fichier template dont les étiquettes ont été valorisées (optionnel égal à _templateFile_)
- __prefix__ : préfixe de recherche des étiquettes dans le fichier template (optionnel égal à _@_)
- __suffix__ : suffixe de recherche des étiquettes dans le fichier template (optionnel égal à _@_)

<u>Exemple de fonctionnenent</u>

- Fichier template
```xml
<?xml version="1.0" encoding="UTF-8"?>
<Root>
    <balises>
        <balise>@LABEL_ONE@</balise>
        <balise>@LABEL_TWO@</balise>
    </balises>
</Root>
```

- Fichier propriété
```properties
LABEL_ONE=valeur1
LABEL_TWO=valeur2
````

- Fichier target (post-transformation)
```xml
<?xml version="1.0" encoding="UTF-8"?>
<Root>
    <balises>
        <balise>valeur1</balise>
        <balise>valeur2</balise>
    </balises>
</Root>
```

### 3.3 Utilisation de  la fonction de lecture des propriétés d'un POM maven

Pour se faire, il est nécessaire de suivre les étapes suivantes depuis la console web jenkins:
- Se connecter à la console http://ip:port/jenkins
- Se positionner sur la pipeline jenkins pour laquelle une analyze de code webMethods doit être réalisée

- Ajouter les lignes de code suivantes :
```groovy

//-Déclaration de la shared libraruy
@Library('analyzebox@develop') _

pipeline {
    agent any
    stages {
        stage('ReadMavenproperties-with-AnalyzeBox') {
            steps {
                cleanWs() 
                script {
                    //-Appel de la fonction
                    pomProperties = readMavenProperties(
                        pomFile: 'C://DEV//myProject//pom.xml',
                        properties: ['propOne', 'propTwo']
                    )
                    echo 'Pom propertie  propOne = ' + pomProperties["propOne"]
                    echo 'Pom propertie  propTwo = ' + pomProperties["propTwo"]
                }
            }
        }
    }
}

```
avec les paramètres ci-dessous :
- __pomFile__ : fichier pom.xml à lire
- __properties__ : liste des noms de propriétés à lire

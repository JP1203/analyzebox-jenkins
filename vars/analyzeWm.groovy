//- analyze webMethods code by analyzeBox
//- Parameters:
//-     analyzeBoxUserCredentialId: Jenkins credential id for analyzeBox user
//-     subscriptionId: subscription ident
//-     projectId: project ident
//-     packagesFolder: folder that contains packages to analyze
//-     strictAnalyzis: flag indiquant si l'audit est bloquant (true par defaut)
//-     httpProxy: http Url proxy - optional (exmple http://eupgdprxp033.eu.loi.net:8080)
//- Return: true si audit de code présente des erreurs fatales
def call(Map stageParams) {
    echo 'AnalyzeBox - Analyzing packages folder ' + stageParams.packagesFolder
    boolean isFatal = false
    boolean strictAnalyzis = true

    script {
        if (stageParams.strictAnalyzis != null) {
            strictAnalyzis=stageParams.strictAnalyzis
        }
        analyzeBoxSimpleUrl="https://analyzebox-backend-hsfqdie63a-ew.a.run.app/api/analyzebox"
        analyzeBoxUrl = 'url:' + analyzeBoxSimpleUrl;
        analyzeBoxProjectUrl = analyzeBoxUrl + '/subscriptions/' + stageParams.subscriptionId + '/projects/' + stageParams.projectId
        withCredentials([usernamePassword(credentialsId: stageParams.analyzeBoxUserCredentialId, passwordVariable: 'analyzeBoxPassword', usernameVariable: 'analyzeBoxUser')]) {
            protectedPasword = analyzeBoxPassword.replace('\"', '\\\"');
            bodyToken = '{ "email": "' + analyzeBoxUser + '", "password": "' + protectedPasword + '" }';
         }                
 
        response = httpRequest acceptType: 'APPLICATION_JSON', 
            contentType: 'APPLICATION_JSON', 
            httpMode: 'POST',
            httpProxy: stageParams.httpProxy,
            ignoreSslErrors: true,  
            requestBody: bodyToken, 
            url: analyzeBoxUrl + '/authentication/token', 
            validResponseCodes: '200'
                 
        jsonTokenResponse = readJSON text: response.content

        zip zipFile: 'packagesToAnalyse.zip', dir: stageParams.packagesFolder  
                
        response = httpRequest acceptType: 'APPLICATION_JSON', 
            contentType: 'APPLICATION_ZIP', 
            customHeaders: [[maskValue: true, name: 'X-Analyzer-Auth', value: jsonTokenResponse.idToken]],  
            httpMode: 'POST',
            httpProxy: stageParams.httpProxy,
            ignoreSslErrors: true,
            multipartName: 'file', 
            uploadFile: 'packagesToAnalyse.zip', 
            url: analyzeBoxProjectUrl +'/analyzewm', 
            validResponseCodes: '201'

        jsonAuditResponse = readJSON text: response.content
        println("AnalyzeIDREQUEST: " + jsonAuditResponse.idRequest)  
        isFatal = jsonAuditResponse.fatal

		retry(4) {
        	sleep 5
        	response = httpRequest httpMode: 'GET',
            	customHeaders: [[maskValue: true, name: 'X-Analyzer-Auth', value: jsonTokenResponse.idToken]],  
            	httpProxy: stageParams.httpProxy,
            	ignoreSslErrors: true,
            	outputFile: "report.xlsm",
            	url: analyzeBoxProjectUrl + '/reports/' + jsonAuditResponse.idRequest, 
            	validResponseCodes: '200'
         }
         
         archiveArtifacts artifacts: 'report.xlsm', followSymlinks: false
    }

    if( isFatal && strictAnalyzis) {
       error("Fatal error on webMethods code analyzis. Please consult report on ${analyzeBoxSimpleUrl}") 
    }
    return isFatal
}
//- installLiquibase
//      : Installation de  l'uilitaire liquibase
//- Parameters:
//-     liquibase_home: Home directory d'installation du produit
//-     liquibase_overwrite: true si écrasement de la précédente installation
//-     liquibase_version: version liquibase à installer (Optionnel - 4.0.0)
//-     httpProxy: url de connexion au proxy http (Optionnel - format http://<ip>:<port>)

def call(Map stageParams) {
    echo 'Liquibase - automatic install' 
    script {
        liquibase_version='4.0.0'
        if (stageParams.liquibase_version) {
           liquibase_version=stageParams.liquibase_version
        }

        liquibase_overwrite='false'
        if (stageParams.liquibase_overwrite) {
           liquibase_overwrite=stageParams.liquibase_overwrite
        }

        httpProxy=stageParams.httpProxy
        liquibase_home=stageParams.liquibase_home
        liquibase_custom_jars=stageParams.liquibase_custom_jars
        def file = new File(liquibase_home)
        if( file.isDirectory() && liquibase_overwrite == 'false') {
            echo "Directory " + liquibase_home + " allready exist "
        }
        else {
            if( isUnix() ) {
                fileSeparator='/'
            }
            else {
                fileSeparator='\\'
            }

            liquibaseBinary='liquibase.zip'
            liquidbaseUrl='https://github.com/liquibase/liquibase/releases/download/v' + liquibase_version + '/liquibase-' + liquibase_version + '.zip'
            httpRequest httpMode: 'GET',
                    httpProxy: httpProxy,
                    ignoreSslErrors: true,
                    outputFile: liquibaseBinary,
                    url: liquidbaseUrl, 
                    validResponseCodes: '200'
            unzip zipFile: liquibaseBinary, dir: liquibase_home, quiet: true

            echo "Positionnement des librairies custom"
            liquibase_custom_lib=liquibase_home + fileSeparator + 'lib' + fileSeparator + 'custom'
            def liquibase_custom_lib_dir = new File(liquibase_custom_lib);
            liquibase_custom_lib_dir.mkdir()

            for(custom_jar in liquibase_custom_jars) {
                def srcFile = new File(custom_jar)
                def targetFile = new File( liquibase_custom_lib + fileSeparator + srcFile.getName())
                targetFile << srcFile.bytes
 	        } 
        }
    }
}
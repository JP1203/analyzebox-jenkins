//- updateDatabaseWithLiquibase
//      : Mise  à jour de la base de données avec le changelog fourni en paramètre
//- Parameters:
//-     liquibase_home: Home directory d'installation du produit
//-     liquibase_contexts: Contextes associés à l'exécution séparés  par ',' (Optionnel - vide par défaut)
//-     classpath: classpath pour la recherche dy changelogfile (Optionnel - vide par defaut)
//-     changelogfile: Nom du changelog file à traiter
//-     db_server: serveur de la base de données
//-     db_name : nom de l'instance de la base de données
//-     db_user_credentialId : credential ID Jenkins associé à l'utilisateur
//-     ou
//-     db_user : nom de l'utilisateur
//-     db_password : mot de passe de l'utilisateur
//-     db_type: type de base de données (Optionnel - sqlserver par défaut)
//-     db_port: port de la base de données (Optionnel - 1433 par défaut)
def call(Map stageParams) {
    echo 'Liquibase - Update Database' 
    script {
        liquibase_home=stageParams.liquibase_home
        changelogfile=stageParams.changelogfile
        db_server=stageParams.db_server
        db_name=stageParams.db_name
        liquibase_contexts=stageParams.liquibase_contexts
        classpath=stageParams.classpath

        db_user_credentialId=stageParams.db_user_credentialId
        db_user=stageParams.db_user
        db_password=stageParams.db_password

        db_type='sqlserver'
        if (stageParams.db_type) {
           db_type=stageParams.db_type
        }

        db_port='1433'
        if (stageParams.db_port) {
           db_port=stageParams.db_port
        }

        if( isUnix() ) {
            fileSeparator='/'
            classpathSeparator=":"
        }
        else {
            fileSeparator='\\'
            classpathSeparator=";"
        }

        if( db_user_credentialId) {
            withCredentials([usernamePassword(credentialsId: db_user_credentialId, passwordVariable: 'credential_password', usernameVariable: 'credential_user')]) {
               db_user=credential_user
               db_password=credential_password
            }
        }                    
                    
        if( db_type == 'sqlserver') {
            driver='com.microsoft.sqlserver.jdbc.SQLServerDriver'
            jdbcUrl='jdbc:sqlserver://' + db_server + ':' + db_port + ';database=' + db_name + ';'
        }

        liquibase_custom_libraries_path=liquibase_home + fileSeparator + 'lib' + fileSeparator + 'custom'
        if( classpath) {
            liquibase_custom_libraries_path=classpath + classpathSeparator + liquibase_custom_libraries_path
        }
        liquibaseCMD = '@' + liquibase_home + fileSeparator + 'liquibase --driver=' + driver + ' --classpath=' + liquibase_custom_libraries_path + ' --url=' + jdbcUrl + ' --username=' + db_user + ' --password=' + db_password + ' --changeLogFile=' + changelogfile
        if( liquibase_contexts) {
            liquibaseCMD = liquibaseCMD + ' --contexts=' + liquibase_contexts
        }
        liquibaseCMD = liquibaseCMD + ' update'
        bat label: 'updateDatabaseWithLiquibase', 
            returnStatus: true, 
            script: liquibaseCMD
    }
}
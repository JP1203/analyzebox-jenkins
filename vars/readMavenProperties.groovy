//- readMavenProperties
//- Parameters:
//-     pomFile: fichier pom
//-     properties: Liste des propriétés à lire
//- Return : Map<key,value> des propriétés lues
def call(Map stageParams) {
    echo 'readMavenProperties pom file ' + stageParams.pomFile
    def mapProperties = [:]
    pom = readMavenPom file:stageParams.pomFile
    pomProperties =  pom.getProperties()
	for(property in stageParams.properties) {
        value = pomProperties.getProperty(property)
        mapProperties[property] = value
	}        
    return mapProperties
}
//- variableReplace
//- Parameters:
//-     templateFile: fichier template
//-     propertiesFile: fichier contenant les propriétés
//-     targetFile: fichier cible (défaut templateFile)
//-     prefix: prefix de la propriété à remplacer dans le template (défaut @)
//-     suffix: suffixe de la propriété à remplacer dans le template (défaut @)
def call(Map stageParams) {
    echo 'Contextualize file ' + stageParams.templateFile + ' with properties file ' + stageParams.propertiesFile
    script {
        prefix='@'
        if (stageParams.prefix) {
           prefix=stageParams.prefix
        }
        suffix='@'            
        if (stageParams.suffix) {
           suffix=stageParams.suffix
        }
        targetFile=stageParams.templateFile
        if (stageParams.targetFile) {
           targetFile=stageParams.targetFile
        }

        props = readProperties file: stageParams.propertiesFile
        File myTemplateFile = new File(stageParams.templateFile)
		String myTemplateText = myTemplateFile.getText()
		List myList = myTemplateText.findAll( prefix + '[^' + prefix + ']*' + suffix )
		for(param in myList) {
			String subParam = param.substring(1,param.length()-1)
			String propertie = props["${subParam}"]
			if (propertie) {
				myTemplateText = myTemplateText.replaceFirst(param,propertie)
			}					
		}

        File myTargetFile = new File(targetFile)
		myTargetFile.setText(myTemplateText)
    }
}
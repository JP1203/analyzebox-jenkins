//- generateChangeLogWithLiquibase
//      : Génération des changes logs à partir d'une base de données
//- Parameters:
//-     liquibase_home: Home directory d'installation du produit
//-     db_server: serveur de la base de données
//-     db_name : nom de l'instance de la base de données
//-     db_user_credentialId : credential ID Jenkins associé à l'utilisateur
//-     ou
//-     db_user : nom de l'utilisateur
//-     db_password : mot de passe de l'utilsiateur
//-     db_type: type de base de données (Optionnel - sqlserver par défaut)
//-     db_port: port de la base de données (Optionnel - 1433 par défaut)
//-     changelogfile: Nom du changelog file généré (Optionnel - db.changelog-0.0.yaml par défaut)
//-     outputDir: répertoire résultat (Optionnel - workspace courant par défaut)
def call(Map stageParams) {
    echo 'Liquibase - generateChangeLog' 
    script {
        liquibase_home=stageParams.liquibase_home
        db_server=stageParams.db_server
        db_name=stageParams.db_name

        db_user_credentialId=stageParams.db_user_credentialId
        db_user=stageParams.db_user
        db_password=stageParams.db_password

        db_type='sqlserver'
        if (stageParams.db_type) {
           db_type=stageParams.db_type
        }

        db_port='1433'
        if (stageParams.db_port) {
           db_port=stageParams.db_port
        }

        changelogfile='db.changelog-0.0.yaml'
        if (stageParams.changelogfile) {
           changelogfile=stageParams.changelogfile
        }

        outputDir='.'
        if (stageParams.outputDir) {
           outputDir=stageParams.outputDir
        }

        if( isUnix() ) {
            fileSeparator='/'
        }
        else {
            fileSeparator='\\'
        }

        if( db_user_credentialId) {
            withCredentials([usernamePassword(credentialsId: db_user_credentialId, passwordVariable: 'credential_password', usernameVariable: 'credential_user')]) {
               db_user=credential_user
               db_password=credential_password
            }
        }                    
                    
        if( db_type == 'sqlserver') {
            driver='com.microsoft.sqlserver.jdbc.SQLServerDriver'
            jdbcUrl='jdbc:sqlserver://' + db_server + ':' + db_port + ';database=' + db_name + ';'
        }

        liquibase_custom_libraries_path=liquibase_home + fileSeparator + 'lib' + fileSeparator + 'custom'
        liquibaseCMD = '@' + liquibase_home + fileSeparator + 'liquibase --driver=' + driver + ' --classpath=' + liquibase_custom_libraries_path + ' --url=' + jdbcUrl + '  --includeCatalog=false  --includeTablespace=true --dataOutputDirectory=' + outputDir + fileSeparator + 'datas --changeLogFile=' + outputDir + fileSeparator + changelogfile + ' --username=' + db_user + ' --password=' + db_password + ' generateChangeLog'
        bat label: 'liquibase_generateChangeLog', 
            returnStatus: true, 
            script: liquibaseCMD
    }
}
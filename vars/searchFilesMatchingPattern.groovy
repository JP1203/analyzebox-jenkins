//- searchFilesMatchingPattern
//      : Recherche de fichiers respesctant un pattern
//- Parameters:
//-     folder: répertoire de recherche
//-     pattern: pattern de recherche (Optionnel .* par défaut)
//-     isFile: flag de recherche des fichiers (Optionnel true par défaut)
//-     isDirectory: flag de recherche des répertoires (Optionnel true par défaut)
//-     outFormat: format de la liste des réponses (File ou String par défaut)
//- Return :
//      Liste des fichiers trouvés
def call(Map stageParams) {
    echo 'File - Search File' 
    def foundFile = []

    pattern='.*'
    if (stageParams.pattern) {
       pattern=stageParams.pattern
    }

    isFile='true'
    if (stageParams.isFile) {
       isFile=stageParams.isFile
    }

    isDirectory='true'
    if (stageParams.isDirectory) {
       isDirectory=stageParams.isDirectory
    }

    outFormat='File'
    if (stageParams.outFormat) {
       outFormat=stageParams.outFormat
    }
   
    folder=stageParams.folder

    def dir = new File(folder)
    def list = dir.listFiles()
    for( myFile in list) {
       if( myFile.getName().matches(pattern)) {
            if( myFile.isDirectory() && isDirectory) {
                if( outFormat.equalsIgnoreCase('File')) {
                    foundFile = foundFile + myFile
                }
                else {
                foundFile = foundFile + myFile.getAbsolutePath()
                }
            }
            else if( myFile.isFile() && isFile) {
                if( outFormat.equalsIgnoreCase('File')) {
                    foundFile = foundFile + myFile
                }
                else {
                foundFile = foundFile + myFile.myFile.getAbsolutePath()
                }
            }
       }
    }  
    return foundFile
}